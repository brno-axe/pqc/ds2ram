/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stm_rng.h"
#include "elapsed_time.h"
#include "rand.h"
#include "poly.h"
#include "reduce.h"
#include "string.h"
#include "ds2_osi3.h"
#include "fips202.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
typedef struct {
	CRYP_HandleTypeDef hcryp;
	uint32_t pKeyAES2[8];
	uint32_t pInitVectAES2[4];
}aes256ctr_hash_state;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define TIMER_POLY_CK_STD 0
#define TIMER_POLY_CK_AES 1
#define TIMER_POLY_R_STD 2
#define TIMER_POLY_R_AES 3
#define TIMER_COMMIT_STD 4
#define TIMER_COMMIT_AES 5
#define TIMER_POLY_MULL  6
#define TIMER_POLY_NTT  7
#define TIMER_SIGN_STD  8
#define TIMER_SIGN_AES  9
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CRYP_HandleTypeDef hcryp2;
__ALIGN_BEGIN static const uint32_t pKeyAES2[8] __ALIGN_END = {
                            0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000};
__ALIGN_BEGIN static const uint32_t pInitVectAES2[4] __ALIGN_END = {
                            0x00000000,0x00000000,0x00000000,0x00000000};

UART_HandleTypeDef hlpuart1;
DMA_HandleTypeDef hdma_lpuart1_tx;
DMA_HandleTypeDef hdma_lpuart1_rx;

/* USER CODE BEGIN PV */
static DS2_Party 	g_DS2_Data = {0};
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void PeriphCommonClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_LPUART1_UART_Init(void);
static void MX_AES2_Init(void);
/* USER CODE BEGIN PFP */
void aes256ctr_hash_init(aes256ctr_hash_state* state);
void aes256ctr_hash_absorb(aes256ctr_hash_state* state, uint32_t* in, size_t len_in);
void aes256ctr_hash_absorb_nonce(aes256ctr_hash_state* state, uint32_t nonce);
void aes256ctr_hash_squeeze(aes256ctr_hash_state* state, size_t len_out, uint32_t *out);
void aes256ctr_hash_finalize(aes256ctr_hash_state* state);

void sample_normal_from_seed_aes(const uint8_t seed[SEED_BYTES], uint32_t nonce, int32_t mean, int32_t stddev, size_t coeffs_count, int32_t *coeffs);
void poly_uniform_aes(const uint8_t seed[SEED_BYTES], size_t polys_count, size_t nonce, poly_t *poly);
void poly_gen_commit_aes(const uint8_t ck_seed[SEED_BYTES], const uint8_t r_seed[SEED_BYTES], poly_t f[][K]);
void sign_simulation();
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

/* Configure the peripherals common clocks */
  PeriphCommonClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_LPUART1_UART_Init();
  MX_AES2_Init();
  /* USER CODE BEGIN 2 */
  elapsed_time_init();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  poly_t ck_ij = {0};
  poly_t r_ij = {0};
  poly_t F[K][K] = {0};
  uint8_t ck_seed[SEED_BYTES] = {0};
  uint8_t r_seed[SEED_BYTES] = {0};
  uint32_t nonce = 0;

  char msg_buff[80] = {0};

  HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);

  HAL_Delay(1000);

  for(int i = 0; i < 1; i++)
  {
	  memset(msg_buff, 0 , 80);
	  sprintf(msg_buff, "\n\r--- ITERATION %d ---\n\r\0", i);
	  HAL_UART_Transmit(&hlpuart1, (uint8_t*)msg_buff, strlen(msg_buff), 0xff);


	  //------------------------------------------------------------------------------------------------

	  elapsed_time_start(TIMER_POLY_NTT);
	  poly_ntt(&ck_ij, 1);
      elapsed_time_stop(TIMER_POLY_NTT);

	  memset(msg_buff, 0 , 80);
	  sprintf(msg_buff, "TIMER_POLY_NTT    = %u\n\r\0", elapsed_time_max(TIMER_POLY_NTT));
	  HAL_UART_Transmit(&hlpuart1, (uint8_t*)msg_buff, strlen(msg_buff), 0xff);

	  //------------------------------------------------------------------------------------------------

	  elapsed_time_start(TIMER_POLY_MULL);
		for(size_t n = 0; n < _N; n++)
			ck_ij.coeffs[n] += montgomery_reduce((int64_t) ck_ij.coeffs[n] * r_ij.coeffs[n]);
      elapsed_time_stop(TIMER_POLY_MULL);

	  memset(msg_buff, 0 , 80);
	  sprintf(msg_buff, "TIMER_POLY_MULL   = %u\n\r\0", elapsed_time_max(TIMER_POLY_MULL));
	  HAL_UART_Transmit(&hlpuart1, (uint8_t*)msg_buff, strlen(msg_buff), 0xff);

	  //------------------------------------------------------------------------------------------------

	  elapsed_time_start(TIMER_POLY_R_STD);
		nonce = 0;
		//generate r[j][k]
		do {
			nonce++;
			sample_normal_from_seed(r_seed, i+nonce, 0, TC_S, _N, r_ij.coeffs);
		} while(!poly_check_norm(&r_ij, 1, TC_B));
	  elapsed_time_stop(TIMER_POLY_R_STD);

	  memset(msg_buff, 0 , 80);
	  sprintf(msg_buff, "TIMER_POLY_R_STD  = %u\n\r\0", elapsed_time_max(TIMER_POLY_R_STD));
	  HAL_UART_Transmit(&hlpuart1, (uint8_t*)msg_buff, strlen(msg_buff), 0xff);

	  elapsed_time_start(TIMER_POLY_R_AES);
		nonce = 0;
		//generate r[j][k]
		do {
			nonce++;
			sample_normal_from_seed_aes(r_seed, i+nonce, 0, TC_S, _N, r_ij.coeffs);
		} while(!poly_check_norm(&r_ij, 1, TC_B));
	  elapsed_time_stop(TIMER_POLY_R_AES);


	  memset(msg_buff, 0 , 80);
	  sprintf(msg_buff, "TIMER_POLY_R_AES  = %u\n\r\0", elapsed_time_max(TIMER_POLY_R_AES));
	  HAL_UART_Transmit(&hlpuart1, (uint8_t*)msg_buff, strlen(msg_buff), 0xff);

	  //------------------------------------------------------------------------------------------------


	  elapsed_time_start(TIMER_POLY_CK_STD);
	  poly_uniform(ck_seed, 1, 1, (poly_t*) &ck_ij);
	  elapsed_time_stop(TIMER_POLY_CK_STD);

	  memset(msg_buff, 0 , 80);
	  sprintf(msg_buff, "TIMER_POLY_CK_STD = %u\n\r\0", elapsed_time_max(TIMER_POLY_CK_STD));
	  HAL_UART_Transmit(&hlpuart1, (uint8_t*)msg_buff, strlen(msg_buff), 0xff);


	  elapsed_time_start(TIMER_POLY_CK_AES);
	  poly_uniform_aes(ck_seed, 1, 1, (poly_t*) &ck_ij);
	  elapsed_time_stop(TIMER_POLY_CK_AES);

	  memset(msg_buff, 0 , 80);
	  sprintf(msg_buff, "TIMER_POLY_CK_AES = %u\n\r\0", elapsed_time_max(TIMER_POLY_CK_AES));
	  HAL_UART_Transmit(&hlpuart1, (uint8_t*)msg_buff, strlen(msg_buff), 0xff);


	  //------------------------------------------------------------------------------------------------

	  HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_SET);
	  elapsed_time_start(TIMER_COMMIT_AES);
	  poly_gen_commit_aes(ck_seed, r_seed, F);
	  elapsed_time_stop(TIMER_COMMIT_AES);
	  HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_RESET);

	  memset(msg_buff, 0 , 80);
	  sprintf(msg_buff, "TIMER_COMMIT_AES  = %u\n\r", elapsed_time_max(TIMER_COMMIT_AES));
	  HAL_UART_Transmit(&hlpuart1, (uint8_t*)msg_buff, strlen(msg_buff), 0xff);

	  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_SET);
	  elapsed_time_start(TIMER_COMMIT_STD);
	  poly_gen_commit(ck_seed, r_seed, F);
	  elapsed_time_stop(TIMER_COMMIT_STD);
	  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_RESET);

	  memset(msg_buff, 0 , 80);
	  sprintf(msg_buff, "TIMER_COMMIT_STD  = %u\n\r", elapsed_time_max(TIMER_COMMIT_STD));
	  HAL_UART_Transmit(&hlpuart1, (uint8_t*)msg_buff, strlen(msg_buff), 0xff);

	  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_SET);
	  HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_SET);
	  elapsed_time_start(TIMER_SIGN_STD);
	  sign_simulation();
	  elapsed_time_stop(TIMER_SIGN_STD);
	  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_RESET);

	  memset(msg_buff, 0 , 80);
	  sprintf(msg_buff, "TIMER_SIGN_STD    = %u\n\r", elapsed_time_max(TIMER_SIGN_STD));
	  HAL_UART_Transmit(&hlpuart1, (uint8_t*)msg_buff, strlen(msg_buff), 0xff);

  }



  while (1)
  {

	  HAL_Delay(1000);
	  HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV2;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure the SYSCLKSource, HCLK, PCLK1 and PCLK2 clocks dividers
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK4|RCC_CLOCKTYPE_HCLK2
                              |RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.AHBCLK2Divider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.AHBCLK4Divider = RCC_SYSCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief Peripherals Common Clock Configuration
  * @retval None
  */
void PeriphCommonClock_Config(void)
{
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Initializes the peripherals clock
  */
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SMPS;
  PeriphClkInitStruct.SmpsClockSelection = RCC_SMPSCLKSOURCE_HSI;
  PeriphClkInitStruct.SmpsDivSelection = RCC_SMPSCLKDIV_RANGE1;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN Smps */

  /* USER CODE END Smps */
}

/**
  * @brief AES2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_AES2_Init(void)
{

  /* USER CODE BEGIN AES2_Init 0 */

  /* USER CODE END AES2_Init 0 */

  /* USER CODE BEGIN AES2_Init 1 */

  /* USER CODE END AES2_Init 1 */
  hcryp2.Instance = AES2;
  hcryp2.Init.DataType = CRYP_DATATYPE_32B;
  hcryp2.Init.KeySize = CRYP_KEYSIZE_256B;
  hcryp2.Init.pKey = (uint32_t *)pKeyAES2;
  hcryp2.Init.pInitVect = (uint32_t *)pInitVectAES2;
  hcryp2.Init.Algorithm = CRYP_AES_CTR;
  hcryp2.Init.DataWidthUnit = CRYP_DATAWIDTHUNIT_WORD;
  hcryp2.Init.KeyIVConfigSkip = CRYP_KEYIVCONFIG_ALWAYS;
  if (HAL_CRYP_Init(&hcryp2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN AES2_Init 2 */

  /* USER CODE END AES2_Init 2 */

}

/**
  * @brief LPUART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_LPUART1_UART_Init(void)
{

  /* USER CODE BEGIN LPUART1_Init 0 */

  /* USER CODE END LPUART1_Init 0 */

  /* USER CODE BEGIN LPUART1_Init 1 */

  /* USER CODE END LPUART1_Init 1 */
  hlpuart1.Instance = LPUART1;
  hlpuart1.Init.BaudRate = 115200;
  hlpuart1.Init.WordLength = UART_WORDLENGTH_8B;
  hlpuart1.Init.StopBits = UART_STOPBITS_1;
  hlpuart1.Init.Parity = UART_PARITY_NONE;
  hlpuart1.Init.Mode = UART_MODE_TX_RX;
  hlpuart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  hlpuart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  hlpuart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  hlpuart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  hlpuart1.FifoMode = UART_FIFOMODE_DISABLE;
  if (HAL_UART_Init(&hlpuart1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&hlpuart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&hlpuart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&hlpuart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN LPUART1_Init 2 */

  /* USER CODE END LPUART1_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMAMUX1_CLK_ENABLE();
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LED_RED_Pin|LED_YELLOW_Pin|LED_GREEN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : LED_RED_Pin LED_YELLOW_Pin LED_GREEN_Pin */
  GPIO_InitStruct.Pin = LED_RED_Pin|LED_YELLOW_Pin|LED_GREEN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : LED_BLUE_Pin */
  GPIO_InitStruct.Pin = LED_BLUE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_BLUE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PB6 PB7 */
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */
void aes256ctr_hash_init(aes256ctr_hash_state* state)
{

	memset(state->pKeyAES2, 0 ,sizeof(state->pKeyAES2));
	memset(state->pInitVectAES2, 0 ,sizeof(state->pInitVectAES2));
	state->hcryp.Instance = AES2;
	state->hcryp.Init.DataType = CRYP_DATATYPE_32B;
	state->hcryp.Init.KeySize = CRYP_KEYSIZE_256B;
	state->hcryp.Init.pKey = (uint32_t *)state->pKeyAES2;
	state->hcryp.Init.pInitVect = (uint32_t *)state->pInitVectAES2;
	state->hcryp.Init.Algorithm = CRYP_AES_CTR;
	state->hcryp.Init.DataWidthUnit = CRYP_DATAWIDTHUNIT_WORD;
	state->hcryp.Init.KeyIVConfigSkip = CRYP_KEYIVCONFIG_ALWAYS;
	  if (HAL_CRYP_Init(&(state->hcryp)) != HAL_OK)
	  {
		Error_Handler();
	  }
}

void aes256ctr_hash_absorb(aes256ctr_hash_state* state, uint32_t* in, size_t len_in)
{
	static int  i = 0;
	int  j = 0;
	for(j = 0; j < len_in; j++){
		state->pKeyAES2[i] ^= in[j];
		i = (i+1) % 8;
	}

}

void aes256ctr_hash_absorb_nonce(aes256ctr_hash_state* state, uint32_t nonce)
{
	state->pInitVectAES2[0] ^= nonce;
	state->pInitVectAES2[1] ^= nonce << 8;
	state->pInitVectAES2[2] ^= nonce << 16;
	state->pInitVectAES2[3] ^= (nonce << 24 )| (nonce >> 8 );
}


void aes256ctr_hash_squeeze(aes256ctr_hash_state* state, size_t len_out, uint32_t *out)
{

	HAL_CRYP_Encrypt(&(state->hcryp), out, len_out, out, 0xff);

}

void aes256ctr_hash_finalize(aes256ctr_hash_state* state)
{/*
  if (HAL_CRYP_DeInit(&(state->hcryp)) != HAL_OK)
  {
	Error_Handler();
  }*/
}

// Expects coeffs_count < N and coeffs_count is even.
void sample_normal_from_seed_aes(const uint8_t seed[SEED_BYTES], uint32_t nonce, int32_t mean, int32_t stddev, size_t coeffs_count, int32_t *coeffs) {

    uint8_t data[4 * _N] = {0};
    uint32_t u1, u2;
/*
    aes256ctr_hash_state state;

    aes256ctr_hash_init(&state);
    aes256ctr_hash_absorb(&state, (uint32_t*)seed, SEED_BYTES/4);
    aes256ctr_hash_absorb_nonce(&state, nonce);
    aes256ctr_hash_squeeze(&state, _N, (uint32_t*)data);
    aes256ctr_hash_finalize(&state);
*/

    poly_uniform_aes(seed, 1, nonce, (poly_t*)data);

    for (size_t i = 0; i < coeffs_count / 2; i++) {
        u1 = data[8 * i] | (((uint32_t) data[8 * i + 1]) << 8) | (((uint32_t) data[8 * i + 2]) << 16) | (((uint32_t) data[8 * i + 3]) << 24);
        u2 = data[8 * i + 4] | (((uint32_t) data[8 * i + 5]) << 8) | (((uint32_t) data[8 * i + 6]) << 16) | (((uint32_t) data[8 * i + 7]) << 24);

        uniform_to_normal((double) u1 / UINT32_MAX, (double) u2 / UINT32_MAX, 0, stddev, &coeffs[2 * i], &coeffs[2 * i + 1]);
    }
}


void poly_uniform_aes(const uint8_t seed[SEED_BYTES], size_t polys_count, size_t nonce, poly_t *poly) {

	aes256ctr_hash_state state;
    uint8_t data[24 * _N];
    uint16_t count;
    int32_t coeff;

    for (size_t i = 0; i < polys_count; i++) {

        aes256ctr_hash_init(&state);

        aes256ctr_hash_absorb(&state, (uint32_t*)seed, SEED_BYTES/4);
        aes256ctr_hash_absorb_nonce(&state, i+nonce);

        count = 0;

        do {
        	aes256ctr_hash_squeeze(&state, sizeof(data)/4, (uint32_t*)data);

            for (size_t j = 0; j < _N; j++) {
                coeff = ((data[3 * j] << 16) | (data[3 * j + 1] << 8) | data[3 * j]) & 0x7fffff;

                if (coeff < _Q)
                    poly[i].coeffs[count++] = coeff;

                if (count == _N)
                    break;
            }
        } while (count != _N);

        aes256ctr_hash_finalize(&state);
    }

}

void poly_gen_commit_aes(const uint8_t ck_seed[SEED_BYTES], const uint8_t r_seed[SEED_BYTES], poly_t f[][K])
{
    poly_t r_kj = {0};
    poly_t ck_ik = {0};
    uint32_t nonce = 0;

    memset(f, 0, K*K*_N);

    for(size_t k = 0; k < TC_COLS; k++) {
    	for (size_t j = 0; j < K; j++) {
    		nonce = 0;
			//generate r[j][k]
    		do {
    			nonce++;
    			sample_normal_from_seed_aes(r_seed, j*TC_COLS+k+nonce, 0, TC_S, _N, r_kj.coeffs);
    		} while(!poly_check_norm(&r_kj, 1, TC_B));

    		for(size_t i = 0; i < K; i++) {

        		//generate ck[i][k]
        		poly_uniform_aes(ck_seed, 1, i*TC_COLS+k, (poly_t*) &ck_ik);

        		// f[i][j] += r[j][k] * ck[i][k]
    			for(size_t n = 0; n < _N; n++)
    			    f[i][j].coeffs[n] += montgomery_reduce((int64_t) ck_ik.coeffs[n] * r_kj.coeffs[n]);
    		}
    	}
    }
    poly_reduce((poly_t*)f, K*K);
    poly_invntt_tomont((poly_t*)f, K*K);
}

void sign_simulation()
{
	uint8_t msg_buff[256] = {0xAA};
	//private key
	static poly_t 		t0[K] = {0};
	static poly_t 		s1[L] = {0};
	static poly_t 		s2[K] = {0};

	static uint8_t		tr[SEED_BYTES] = {0x44};
	//public key
	static poly_t		A[K][L] = {0};

	//signature
	static uint8_t		rej = 1;
	static uint32_t		nonce_y = 0;
	static uint8_t 		y_seed[SEED_BYTES] = {0};
	static uint8_t 		r_seed[SEED_BYTES] = {0};
	static uint8_t 		ck_seed[SEED_BYTES] = {0};
	static poly_t		y1[L] = {0};
	static poly_t		y2[K] = {0};
	static poly_t		z1[L] = {0};
	static poly_t		z2[K] = {0};
	static uint8_t		c[SEED_BYTES] = {0};
	static poly_t		poly_c = {0};
	static poly_t		w[K] = {0};
	static poly_t		Fi[K][K] = {0};

	poly_t y1_[L] = {0};
	poly_t cs1[L] = {0};
	poly_t cs2[K] = {0};

	poly_t t0_[K] = {0};
	uint32_t* y_seed_ptr;
	uint32_t* r_seed_ptr;

	y_seed_ptr = (uint32_t*)y_seed;
	r_seed_ptr = (uint32_t*)r_seed;
	for(int i = 0; i < (SEED_BYTES / sizeof(uint32_t)); i++)
	{
		RNG_GenerateRandomInt(y_seed_ptr);
		y_seed_ptr++;

		RNG_GenerateRandomInt(r_seed_ptr);
		r_seed_ptr++;
	}

	keccak_state_t state;
	keccak_init(&state);
	shake256_absorb(&state, msg_buff, 256);
	shake256_absorb(&state, tr, SEED_BYTES);
	shake256_finalize(&state);
	shake256_squeeze(&state, SEED_BYTES, ck_seed);

	//generate y1 and y2
	poly_normal(y_seed, nonce_y, SIGMA, L, y1);
	nonce_y += L;
	poly_normal(y_seed, nonce_y, SIGMA, K, y2);
	nonce_y += K;

	// Compute w_n = (A | I) * y_n
	poly_copy(y1, L, y1_);
	poly_ntt(y1_, L);

	poly_product(A, y1_, w);
	poly_reduce(w, K);
	poly_invntt_tomont(w, K);

	poly_add(w, y2, K, w);
	poly_freeze(w, K);

	poly_gen_commit(ck_seed, r_seed, Fi);
	poly_add(&Fi[1], w, K, &Fi[1]);

	poly_freeze((poly_t*)Fi, K*K);
	poly_pack(TC_L, (poly_t*) Fi, K * K, g_DS2_Data.fi_commit);


	//////////////////

	poly_challenge(c, &poly_c);

	poly_copy(s1, L, cs1);
	poly_copy(s2, K, cs2);

	poly_ntt(cs1, L);
	poly_ntt(cs2, K);
	poly_ntt(&poly_c, 1);

	poly_mul_pointwise(cs1, &poly_c, L, cs1);
	poly_mul_pointwise(cs2, &poly_c, K, cs2);

	poly_reduce(cs1, L);
	poly_reduce(cs2, K);

	poly_invntt_tomont(cs1, L);
	poly_invntt_tomont(cs2, K);

	poly_add(cs1, y1, L, z1);
	poly_add(cs2, y2, K, z2);

	rej = 0;
	rej = poly_reject(z1, z2, cs1, cs2);

	poly_copy(t0, K, t0_);
	poly_ntt(t0_, K);
	poly_mul_pointwise(t0_, &poly_c, K, t0_);
	poly_reduce(t0_, K);
	poly_invntt_tomont(t0_, K);

	poly_sub(z2, t0_, K, z2);

    poly_center(z1, L);
    poly_center(z2, K);

    rej |= !poly_check_norm(z1, L, B);
    rej |= !poly_check_norm(z2, K, B);

    poly_addq(z1, L);
    poly_pack(TC_L, z1, L, g_DS2_Data.zi_1_val);

    poly_addq(z2, K);
    poly_pack(TC_L, z2, L, g_DS2_Data.zi_2_val);

}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
	  HAL_Delay(500);
	  HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
