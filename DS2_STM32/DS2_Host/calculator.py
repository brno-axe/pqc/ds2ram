import math

def memory_calculator_orig(K, L, N, T, n):
    print("\n\n --- K = {}, L = {}, N = {}, T = {}, n = {} ---".format(K, L, N, T, n))

    M_static = 4*N*(K*(L+(n+2)) + L + + (n+1)*K*K)
    M_stack = 4*N*((K+1)*T + 5*L + 7*K + 1) + 3*N*K*(2+K)
    M_heap = (K+1)*3*N*T
    M_sign = 4*N*(K+L+K*T)

    print("\n-- old implementation --")
    print("M_static = {} KB".format(M_static /1024))
    print("M_stack = {} KB".format(M_stack / 1024))
    print("M_heap = {} KB".format(M_heap / 1024))
    print("M_sign = {} KB".format(M_sign / 1024))
    print("Total = {} KB".format((M_static + M_stack + M_heap + M_sign) / 1024))

    M_static1 = 4*N*(K*(n+1) + (n+2)*K*K) #*(n+1)
    M_sign1 = 4 * N * (K + L)
    M_stack1 = 4*N*(5*L + 7*K  + 6) + 3*N*K*(2+K)
    M_heap1 = 0

    print("\n-- Modification 1 --")
    print("M_static = {} KB".format(M_static1 /1024))
    print("M_stack = {} KB".format(M_stack1 / 1024))
    print("M_heap = {} KB".format(M_heap1 / 1024))
    print("M_sign = {} KB".format(M_sign1 / 1024))
    print("Total = {} KB".format((M_static1 + M_stack1 + M_heap1 + M_sign1) / 1024))

    M_static2 = 4*N*(5*L + 7*K + 3)
    M_sign2 = 4 * N * (K + L)
    M_stack2 = 4*N*(2*K + L + K*K + 3) + 3*N*(2*K + L + K*K)
    M_heap2 = 0

    print("\n-- Modification 2 --")
    print("M_static = {} KB".format(M_static2 / 1024))
    print("M_stack = {} KB".format(M_stack2 / 1024))
    print("M_heap = {} KB".format(M_heap2 / 1024))
    print("M_sign = {} KB".format(M_sign2 / 1024))
    print("Total = {} KB".format((M_static2 + M_stack2 + M_heap2 + M_sign2) / 1024))


    #M_static3 = 4*N*(5*L + 7*K + 3)
    #M_sign3 = 4 * N * (K + L)
    #M_stack3 = 4*N*(2*K + L + K*K + 3) + 3*N*(2*K + L + K*K)
    #M_heap3 = 0

    #print("\n-- Modification 3 --")
    #print("M_static = {} KB".format(M_static3 / 1024))
    #print("M_stack = {} KB".format(M_stack3 / 1024))
    #print("M_heap = {} KB".format(M_heap3 / 1024))
    #print("M_sign = {} KB".format(M_sign3 / 1024))
    #print("Total = {} KB".format((M_static3 + M_stack3 + M_heap3 + M_sign3) / 1024))
    

def DS2_parameters(K, L, N, gamma, eta , kappa, alpha , n):
    
    print("\n\n-- DS2 parameters --")
    t = 12
    T = kappa * eta * math.sqrt(N*(L+K))
    sigma = alpha * T * n
    B = gamma * sigma * math.sqrt((L + K) * N)  #// maximum L^2-norm of signature z (B = gamma * sigma * sqrt((l + k) * N))
    M = math.exp(t/alpha + 1/(2*alpha^2))
    print("T = ", T)
    print("sigma = ", sigma)
    print("B = ", B)
    print("M = ", M)



for i in range(2, 8):
    memory_calculator_orig(K=i, L=i, N=256, T=69, n=2)
    memory_calculator_orig(K=i+1, L=i, N=256, T=69, n=2)


DS2_parameters(L=4, K=4, N=256, gamma=2, eta=5, kappa=60, alpha=22, n=2)

DS2_parameters(L=4, K=4, N=256, gamma=2, eta=5, kappa=60, alpha=55, n=5)

DS2_parameters(L=4, K=4, N=256, gamma=2, eta=5, kappa=60, alpha=110, n=10)

DS2_parameters(L=4, K=4, N=256, gamma=2, eta=5, kappa=60, alpha=220, n=20)

DS2_parameters(L=4, K=4, N=256, gamma=2, eta=5, kappa=60, alpha=550, n=50)